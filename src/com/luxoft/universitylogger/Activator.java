package com.luxoft.universitylogger;

import org.osgi.framework.*;

/**
 * Class extends org.osgi.framework.BundleActivator. 
 * Provides activator for UniversityLogger bundle
 */
public class Activator implements BundleActivator {
	private static BundleContext context;

	/**
	 * Method returns BundleContext
	 * 
	 * @return BundleContext
	 */
	public static BundleContext getContext() {
		return context;
	}

	@Override
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}
}

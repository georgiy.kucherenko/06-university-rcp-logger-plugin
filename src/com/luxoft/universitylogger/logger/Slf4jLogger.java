package com.luxoft.universitylogger.logger;

import com.luxoft.universitylogger.api.ILogger;
import org.slf4j.Logger;

/**
 * Wrapping class. Wraps slf4j logger and implements 
 * com.luxoft.universitylogger.api.ILogger interface of 
 * UniversityLogger bundle
 */
public class Slf4jLogger implements ILogger {
	private Logger logger;

	/**
	 * Public constructor
	 * 
	 * @param logger - SLF4J logger
	 */
	public Slf4jLogger(Logger logger) {
		this.logger = logger;
	}

	@Override
	public void debug(String message) {
		logger.debug(message);
	}

	@Override
	public void trace(String message) {
		logger.trace(message);
	}

	@Override
	public void info(String message) {
		logger.info(message);
	}

	@Override
	public void warn(String message) {
		logger.warn(message);
	}

	@Override
	public void error(String message) {
		logger.error(message);
	}

	@Override
	public void error(String message, Throwable e) {
		logger.error(message, e);
	}
}
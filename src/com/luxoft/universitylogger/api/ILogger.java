package com.luxoft.universitylogger.api;

/**
 * Interface for Logger that is used in UniversityLogger bundle
 */
public interface ILogger {

	/**
	 * log debug messages.
	 * 
	 * @param message to be logged
	 */
	void debug(String message);

	/**
	 * log trace messages.
	 * 
	 * @param message to be logged
	 */
	void trace(String message);

	/**
	 * log info messages.
	 * 
	 * @param message to be logged
	 */
	void info(String message);

	/**
	 * log warn messages.
	 * 
	 * @param message to be logged
	 */
	void warn(String message);

	/**
	 * log error messages.
	 * 
	 * @param message to be logged
	 */
	void error(String message);

	/**
	 * log Throwable objects (typically exceptions).
	 * 
	 * @param message to be logged
	 * @param e throwable
	 */
	void error(String message, Throwable e);
}

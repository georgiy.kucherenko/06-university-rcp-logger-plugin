package com.luxoft.universitylogger.api;

import java.io.File;
import java.net.URL;
import java.util.*;

import com.luxoft.universitylogger.Activator;
import com.luxoft.universitylogger.logger.Slf4jLogger;
import org.apache.log4j.PropertyConfigurator;


/**
 * Factory that is configuring and providing loggers.
 */
@SuppressWarnings("rawtypes")
public abstract class LoggerFactory {
	private static boolean isConfigured;
	private static Map<Class, ILogger> loggersRepository;
	private static final String LOG4J_FILENAME = "log4j.properties";


	/**
	 * Method configuring and providing logger
	 * 
	 * @param classname - class in which the logger will be used
	 * @return - ILogger instance
	 */
	public static ILogger getLogger(Class classname) {
		configure();
		if (loggersRepository.containsKey(classname)) {
			return loggersRepository.get(classname);
		} else {
			ILogger slf4jLogger = new Slf4jLogger(org.slf4j.LoggerFactory.getLogger(classname));
			loggersRepository.put(classname, slf4jLogger);
			return slf4jLogger;
		}
	}

	private synchronized static void configure() {
		if (isConfigured) {
			return;
		}
		isConfigured = true;
		loggersRepository = new HashMap<Class, ILogger>();

		File file = new File(LOG4J_FILENAME);
		if (file.exists()) {
			PropertyConfigurator.configure(file.getAbsolutePath());
		} else {
			URL entry = Activator.getContext().getBundle().getEntry(LOG4J_FILENAME);
			PropertyConfigurator.configure(entry);
		}
	}
}